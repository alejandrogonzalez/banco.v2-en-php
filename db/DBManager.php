<?php

require_once('../config/config.php');

class DBManager extends PDO{
    public $server = SERVER;
    public $user = USER;
    public $pass = PASSWORD;
    public $port = PORT;
    public $db = DB;
    private $conexion ;

    public function __construct(){
        $this->conectar();
    }

    private final function conectar(){
        $conexion = null;

        try{
            if(is_array(PDO::getAvailableDrivers())){
                if(in_array('pgsql',PDO::getAvailableDrivers())){
                    error_log('dbmanager');
                    $conexion = new PDO("pgsql:host=$this->server;port=$this->port;dbname=$this->db;user=$this->user;password=$this->pass");
                    //$conexion = new PDO("$this->server,$this->port,$this->db,$this->user,$this->pass");
                }else{
                    throw new PDOException("ERROR");
                }
            }
        }catch (PDOException $e){
            echo $e -> getMessage();
        }
        $this->setConexion($conexion);
    }
    public final function setConexion($conexion){
        $this->conexion = $conexion;
    }

    public final function getConexion(){
        return $this->conexion;
    }

    public final function cerrarConexion(){
        $this->setConexion(null);
    }

}


?>
