<?php


function validateRegister(){
    $message='';
    /*if (!validateNombre($_POST['nombre'])){
        $_POST['nombre'] = '';
        $message= $message."Nombre Inconrrecto  <br/>";
    }

    if(!validateApellido($_POST['apellido'])){
        $_POST['apellido'] = '';
        $message = $message."Apellido Incorrecto <br/>";
    }

    if(!validateFecha($_POST['fecha'])){
        $_POST['fecha'] = '';
        $message = $message."Fecha incorrecta <br/>";
    }

    if(!validateDNI($_POST['dni'])){
        $_POST['dni'] = '';
        $message = $message."DNI Incorrecto <br/>";
    }

    if(!validateTelefono($_POST['tel'])){
        $_POST['tel'];
        $message = $message."Teléfono Incorrecto <br/>";
    }

    if(!validateEmail($_POST['email'])){
        $_POST['email'];
        $message = $message."E-mail Incorrecto <br/>";
    }

    if(!validatePassword($_POST['pass'])){
        $_POST['pass'];
        $message = $message."Contraseña Incorrecta<br/>";
    }

    if(!validateComprovacion()){
        $message = $message."La contraseña no coincide <br/>";
    }*/

    return $message;
}

function validateNombre(){
    if(ctype_alpha(str_replace(' ','',$_POST['nombre']))){
        return true;
    }else{
        return false;
    }
}

function validateApellido(){
    if(ctype_alpha(str_replace(' ','',$_POST['apellido']))){
        return true;
    }else{
        return false;

    }
}

/*function validateFecha($fecha){
    $edadUsuario = new DateTime($_POST['fecha']);

    $fechaActual = $fecha("d-m-y");
    $fechaActual = new DateTime($fechaActual);

    $intervaloTiempo = $edadUsuario -> diff($fechaActual);

    $edad = $intervaloTiempo;

    if($edad >= 18){
        return true;
    }else{
        return false;
    }

}*/

function validateDNI($dni){
    $letra = substr($dni, -1);
    $numeros = substr($dni,0,-1);

    if(substr("TRWAGMYFPDXBNJZSQVHLCKE",$numeros % 23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8){
        return true;
    }else{
        return false;
    }


}

function validateTelefono($value){
    $value = trim($value);

    if($value == ' '){
        return true;
    }
    else{
        if(preg_match('/^\(?[0-9]{3}\)?[-. ]?[0-9]{3}[-. ]?[0-9]{4}$/',$value)){
            return preg_replace('/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/', '($1) $2-$3', $value);
        }else{
            return false;
        }
    }

}


function validateEmail($str){

    $matches = null;
    return (1 === preg_match('/^[A-z0-9\._-]+@[A-z0-9][A-z0-9-]*(\.[A-z0-9_-]+)*\.([A-z]{2,6})$/', $str, $matches));

}


function validatePassword($pass){

    $pass = $_POST['pass'];

    $repetirPass = $_POST['repetirPass'];

    if(strlen($pass) >= '8' && preg_match("#[0-9]+#",$pass) && preg_match("#[A-Z]+#",$pass) && preg_match("#[a-z]+#") && $pass === $repetirPass){
        return true;
    }
    return false;


}

?>