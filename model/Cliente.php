<?php


class Cliente{
    private $nombre;
    private $apellidos;
    private $fecha;
    private $genero;
    private $email;
    private $telephone;
    private $dni;
    private $password;
    private $image;

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Cliente constructor.
     * @param $nombre
     * @param $apellidos
     * @param $fecha
     * @param $genero
     * @param $email
     * @param $telephone
     * @param $dni
     * @param $password
     */
    public function __construct($nombre, $apellidos, $fecha, $genero, $email, $telephone, $dni, $password,$image)
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->fecha = $fecha;
        $this->genero = $genero;
        $this->email = $email;
        $this->telephone = $telephone;
        $this->dni = $dni;
        $this->password = $password;
        $this->image = $image;

    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function setNombre($nombre){
        $this -> nombre = $nombre;
    }

    public function setApellidos($apellidos){
        $this -> apellidos = $apellidos;
    }
    public function getApellidos(){
        return $this -> apellidos ;
    }
    public function getFecha(){
        return $this -> fecha;
    }

    public function setFecha($fecha){
        $this -> fecha = $fecha;
    }

    public function getGenero(){
        return $this -> genero;
    }

    public function getTelefono(){
        return $this -> telephone;
    }

    public function setTelefono($telephone){
        $this -> $telephone = $telephone;
    }

    public function getDni(){
        return $this -> dni;
    }

    public function setDni($dni){
        $this -> dni = $dni;
    }

    public function getEmail(){
        return $this -> email;
    }

    public function setEmail($email){
        $this -> email = $email;
    }

    public function getPassword(){
        return $this->password;
    }

    public function setPassword($password){
        $this -> password = $password;
    }


}

?>