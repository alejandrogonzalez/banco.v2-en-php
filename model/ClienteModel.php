<?php
require_once('../db/DBManager.php');
use DBManager;

function insertCliente($cliente){

    $manager = new DBManager();

    try{
        $sql = "INSERT INTO cliente(nombre,apellidos,nacimiento,sexo,telefono,dni,email,password) 
        VALUES (:nombre,:apellidos,:fecha,:genero,:telephone,:dni,:email,:password)";

        $password = password_hash($cliente -> getPassword(),PASSWORD_DEFAULT,['cost' => 10]);

        $stmt = $manager -> getConexion()->prepare($sql);
        $stmt -> bindParam(':nombre',$cliente->getNombre());
        $stmt -> bindParam(':apellidos',$cliente->getApellidos());
        $stmt -> bindParam(':fecha',$cliente->getFecha());
        $stmt -> bindParam(':genero',$cliente->getGenero());
        $stmt -> bindParam(':telephone',$cliente->getTelefono());
        $stmt -> bindParam(':dni',$cliente->getDni());
        $stmt -> bindParam(':email',$cliente->getEmail());
        $stmt -> bindParam(':password',$password);

        if($stmt -> execute()){
            echo "todo OK";
        }else{
            echo "MAL";
        }

    }catch (PDOException $e){
        echo $e -> getMessage();
    }
}


function getUserHash($dni){
    $conexion = new DBManager();
    try{
        $sql = "SELECT * FROM cliente WHERE dni=:dni";
        $stmt = $conexion -> getConexion() -> prepare($sql);
        $stmt -> bindParam(':dni', $dni);
        $stmt -> execute();
        $result = $stmt -> fetchAll(PDO::FETCH_ASSOC);
        return $result[0]['password'];
    }catch (PDOException $e){
        echo $e -> getMessage();
    }
}

function selectCliente(){
    $manager = new DBManager();
    try{
        $sql="SELECT * FROM cliente WHERE id=7";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $obj = new Cliente($result[0]['nombre'],$result[0]['nacimiento'],$result[0]['apellidos'],$result[0]['sexo'],$result[0]['email'],$result[0]['telefono'],$result[0]['dni'],$result[0]['password'],$result[0]['image']);
        return $obj;
    }catch ( PDOException $e){
        echo $e->getMessage();
    }

}

function updateCliente($image){
    $manager = new DBManager();
    try{
        $sql = "UPDATE cliente SET image=:img WHERE id = 7";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt -> bindParam(':img',$image,PDO::PARAM_LOB);
        $stmt -> execute();
    }catch (PDOException $e){
        error_log('update cliente' .  $e->getMessage());
    }
}


?>