<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>query</title>
</head>
<body>
<h1 class="text-secondary border text-center">Query</h1>
<nav class="nav">
    <a class="nav-link" href="profile.php">Profile</a>
    <a class="nav-link" href="init.php">Init</a>
    <a class="nav-link" href="transfer.php">Transfer</a>
    <a class="nav-link" href="logout.php">Logout</a>
</nav>
<form action="../controller/controller.php" method="post">
    <div class="form-group col">
        <label for="pass">Transacciones:</label>
        <ul class="list-group">
            <li class="list-group-item">Transacción</li>
            <li class="list-group-item">Transacción</li>
            <li class="list-group-item">Transacción</li>
            <li class="list-group-item">Transacción</li>
        </ul>
    </div>
    <select class="custom-select" name="filterBankingTransaction">
        <option selected>Elige tu opción</option>
        <option value="1">Todas</option>
        <option value="2">Recibidas</option>
        <option value="3">Enviadas</option>
    </select>
</form>
</body>

</html>

<?php
session_start();
if (isset($_SESSION['saldo']))
    echo $_SESSION['saldo'];
?>
