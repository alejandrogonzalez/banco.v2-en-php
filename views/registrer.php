

<?php require_once('../helper/i18n.php'); ?>
<html>

<style>

    .formulario{
        display: flex;
        justify-content: center;
        margin: 200px;
        background: lightblue;
        border: 1px solid blue;
        border-radius: 10px;
        width: 20%;
        margin: auto;
        padding: 10px;
    }


</style>
<head>
    <title><?php echo _("Registro"); ?> </title>

</head>
<body>
<?php require_once('header.php');?>
<?php echo _('Registro') ; ?>
<div class="formulario">
    <form action ="./../controller/controller.php" method = "post" name = "frm">

        <input type="text" id ="nombre" name = "nombre" placeholder="Nombre" value="<?php if (isset($_POST['nombre'])) echo $_POST['nombre']?>"><br/><br/>

        <input type = "text" id = "apellido" name = "apellido" placeholder ="Apellido" value="<?php if (isset($_POST['apellido'])) echo $_POST['apellido']?>"><br/><br/>

        <label for = "genero">Género</label><br/>
        <select name = "genero">
            <option value = "1">Masculino</option>
            <option value = "2">Femenino</option>
            <option value = "3">No especificar</option>
        </select><br><br>

        <label for ="fecha">Introduce tu fecha de nacimiento</label><br/>
        <input type = "date" id="fecha" name="fecha" value = "dd-mm-aa"><br/><br/>

        <input type = "text" id="dni" name="dni" placeholder="DNI" value= "<?php if (isset($_POST['dni']))echo $_POST['dni']?>"><br/><br/>

        <input type = "tel" id="telephone" name="telephone" placeholder="Teléfono" value="<?php if (isset($_POST['tel']))echo $_POST['tel']?>"><br/><br/>
        <input type="hidden" value="register" name="control"/>

        <input type="email" id="mail" name="mail" placeholder="E-mail" value="<?php if(isset($_POST['email']))echo $_POST['email']?>"><br/><br/>


        <input type="password" id="pass" name="pass" placeholder="Contraseña" value="<?php if(isset($_POST['pass']))echo $_POST['pass']?>"><br/><br/>

        <input type="comprovacion" id="comprovacion" name="comprovacion" placeholder="Repita su contraseña" value="<?php if(isset($_POST['comprovacion']))echo $_POST['comprovacion']?>"><br/><br/>

        <input type="submit" value="submit" name="submit"/>
</form>
</div>

<?php

if( isset($_POST['valid']))
    echo $_POST['valid'];

if (isset($_POST['message']))
    echo $_POST['message'].'<br/>';


if (isset($datos['algo']))
    echo $datos['algo'].'<br/>';


if (isset($_GET['a']))
    echo $_GET['a'].'<br/>';

?>


</body>


</html>